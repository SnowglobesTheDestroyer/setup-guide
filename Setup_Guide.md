Setup Concept:
==============

> Plug n' Play laptop home setup.

> Basically, you set your laptop down on your desk, and connect a few cables-<br>
> And BOOM! Impromptu Desktop-like setup

> Let's try to MinMax your money to comfort ratio with a laptop, essentially

---
<br>

### What You'll Need:

+ Mouse:<br>
  >Razer Viper Ambidextrous<br>
  >!!!!!!!NOT THE ULTIMATE!!! IT"S MORE EXPENSIVE AND YOU DIDN"T WANT THAT(at least when I asked you)!!!!!!!!!!!!
+ Mousepad:<br>
  >soft<br>
  >(optional) made by one of them big gaming companies[^biggaming]<br>
  >(optional) you'll likely want one decently sized (30x35cm or larger)<br>
  >(optional) sewn edges
+ Keyboard:<br>
  >Cheap and sturdy USB keyboard [the more it flexes and twists, the worse].<br>
  >if someone asks Rubber Dome. NOT MECHANICAL.<br>
  >look for something chunky and pray that all the buttons work. try and flex and twist it.<br>
  >you can get a mechanical, however they *tend* to be more expensive and non essential. you get a good rubber one and you're golden
+ Screen:<br>
  >Cheap, 1080P[^1080p]<br>
  >60FPS<br>
  >HDMI-abled (DVI *can* work but there's extra hassle[^dvi]. do NOT attempt other connectors[^vga])
+ 3.5mm audio jack splitter: <br>
  >The laptop has a combo Mic+Headset like on a phone.<br>
  >you have to split them.
+ Microphone:<br>
  >There are 3 types of microphones:
  >>USB<br>
  >>3.5mm<br> 
  >>XLR. **DO NOT GET AN XLR!**
  <br>
  <br>
  
### What You'll Additionally Want:

* USB hub[^hub]:<br>
  > the specs: 
    > 1. It MUST connect to laptop via standard USB (aka type-A). Under no circumstances get USB type-C!!!
    // <br> 
    > 2. ~~Said USB connection should be USB 3.0, 3.1 or 3.2 (doesn't matter which Generation).~~  Unsure on this one. USB 2.0 may be better for some things.
    // <br> 
    > 3. It must be a POWERED Hub. this means it gets external power because getting all of that power through the laptop for many devices introduces problems
  
  > brands that have good reputation:
  >> Sabrent<br>
  >> Anker<br>
  >> AUKEY<br> 
  >> TP-Link<br>


* Headset :<br>
  > [imo, a Sound Isolating headset.	40-300$ IE lotsa wiggle room]
 
  >asd
  
* Controller :<br>
  > [PS3, PS4, XB360, XB1, something else] (Ideally not a ps3 controller. Big hassle to setup and even then, still clunk) (XB are the easiest to setup by FAR)

* Controller wireless adapter:<br>
  * Mayflash 4-port GC controller Adapter
  * Xbox Adapter (**Only if you get and XB controller**)




[^biggaming]: **Big Gaming Compaines:** Razer, Corsair(what I use), SteelSeries, Logitech, Benq-Zowie, etc

[^1080p]: **1080p:** it is Ideal that you get a 1080p display. will make compatability simpler. believe it or not, higher may be worse for you than lower. performance reasons. 

[^dvi]: **DVI&The Extra Hassle:** put simply, you'd have to know to get DVI-I (Dual Link). Then get a converter for DVI-I (DualLink) to HDMI.<br> Not to be confused with DVI-A, DVI-D (Single Link), DVI-D (Dual Link) or DVI-I (Single Link). *ALL OF THESE ARE FUCKING DIFFERENT AND IT PISSES ME OFF*
<br>You definitely *can* use it, and it should be your backup in case you absolutely cannot find an HDMI one. Another backup is DP (DisplayPort). 
[^vga]: **Other Connectors:** Any other than DVI-I and DisplayPort won't convert from HDMI well iirc.  if you're thinking "But Bill of E, you use screens with that blue connecter cable (VGA aka D-SUB 15), why not use that?" From a guy who uses them. Don't. <br> NonMandatory Explanation:
	- Firstly, Remember how I told you the difference between analog and digital? VGA is analog and HDMI is digital, you'd be mixing apples and oranges. ***It's needless added hassle***. Why try and go from one type to the other? especially when the other is quite common
	- Secondly, D-Sub 15 has added a lot of extra headache precisely because it's analog. things like wonky colors and just not connecting. it's never just "huh it's not working. must be a broken port or cable" there's always more to it. You can literally bend the cable and you can change the Color of the image. <br>
   	Why? because it's analog, meaning any added or reduced current on any given internal cable will change the actual color of the display (assuming it's a color cable. else you will probably end up with no image at all)
[^hub]:**USB HUB Disclaimer**: I am pretty sure this isn't an area to skimp out. Afterall, you will be connecting basically ALL of your usb devices through it for your home setup. 
